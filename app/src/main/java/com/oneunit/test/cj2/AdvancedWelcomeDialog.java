package com.oneunit.test.cj2;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.Toast;

import java.io.IOException;

public class AdvancedWelcomeDialog extends Dialog {
    private Context context;
    private EditText editTextName;
    private EditText editTextPricePerMb;
    private EditText[] limits;
    private EditText[] prices;
    private TableRow[] tableRows;
    private Button buttonAddLimit;
    private Button buttonFinish;
    private final int LIMITS_COUNT = 4;
    private int currentLimits = 1;

    public AdvancedWelcomeDialog(Context context){
        super(context);
        this.context = context;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.configure_dialog);
        this.setCanceledOnTouchOutside(false);

        this.editTextName = (EditText) findViewById(R.id.text_name);
        this.editTextPricePerMb = (EditText) findViewById(R.id.text_price_per_mb);

        this.limits = new EditText[LIMITS_COUNT];
        this.prices = new EditText[LIMITS_COUNT];
        this.tableRows = new TableRow[LIMITS_COUNT];

        this.limits[0] = (EditText) findViewById(R.id.text_limit_0);
        this.limits[1] = (EditText) findViewById(R.id.text_limit_1);
        this.limits[2] = (EditText) findViewById(R.id.text_limit_2);
        this.limits[3] = (EditText) findViewById(R.id.text_limit_3);

        this.prices[0] = (EditText) findViewById(R.id.text_price_0);
        this.prices[1] = (EditText) findViewById(R.id.text_price_1);
        this.prices[2] = (EditText) findViewById(R.id.text_price_2);
        this.prices[3] = (EditText) findViewById(R.id.text_price_3);

        this.tableRows[0] = (TableRow) findViewById(R.id.table_row_limit_0);
        this.tableRows[1] = (TableRow) findViewById(R.id.table_row_limit_1);
        this.tableRows[2] = (TableRow) findViewById(R.id.table_row_limit_2);
        this.tableRows[3] = (TableRow) findViewById(R.id.table_row_limit_3);

        this.buttonAddLimit = (Button) findViewById(R.id.button_add_limit);
        this.buttonFinish = (Button) findViewById(R.id.button_finish);

        this.buttonAddLimit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tableRows[currentLimits++].setVisibility(View.VISIBLE);
                if(currentLimits > LIMITS_COUNT - 1){
                    buttonAddLimit.setVisibility(View.GONE);
                }
            }
        });

        this.buttonFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextPricePerMb.getText().toString().matches("") || editTextName.getText().toString().matches("")){
                    Toast.makeText(context, "Please fill all fields", Toast.LENGTH_SHORT).show();
                    return;
                }
                for(int i = 0; i < currentLimits; i++){
                    if(limits[i].getText().toString().matches("") || prices[i].getText().toString().matches("")){
                        Toast.makeText(context, "Please fill all fields", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                String str = Config.CONFIG_FIRST_USE + "=1" +"\n";
                str+= Config.CONFIG_SEND_DATA + "=1" +"\n";
                str+= editTextName.getText().toString() + "\n";
                str+= editTextPricePerMb.getText().toString() + "\n";
                String temp = "0.3";
                if(currentLimits >0 ){
                    temp = "";
                    for(int i = 0; i < currentLimits; i++){
                        if(i == currentLimits - 1){
                            str+=limits[i].getText().toString();
                            temp+=prices[i].getText().toString();
                        }
                        else{
                            str+=limits[i].getText().toString() + "!";
                            temp+=prices[i].getText().toString() + "!";
                        }
                    }
                    str+="\n";
                }
                float overallPrice = 200;
                if(!limits[0].getText().toString().equals("")) {
                    overallPrice = Float.valueOf(editTextPricePerMb.getText().toString()) * Float.valueOf(limits[0].getText().toString());
                }
                str+= overallPrice + "\n";
                str+= "7.2" + "\n";
                str+= "3g" + "\n";
                str+= temp + "\n";

                try{
                    Config config = new Config(context);
                    config.writeToFile(str);
                }
                catch (IOException e){
                    e.printStackTrace();
                }
                dismiss();
            }
        });
    }
}

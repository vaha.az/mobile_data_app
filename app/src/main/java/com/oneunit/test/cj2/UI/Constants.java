package com.oneunit.test.cj2.UI;


public class Constants {
    public static final int DATA_PER_WEEK = 7;
    public static final int DATA_PER_DAY = 24;
    public static final int DATA_PER_MONTH = 30;
}

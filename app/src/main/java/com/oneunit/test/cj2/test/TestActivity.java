package com.oneunit.test.cj2.test;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.oneunit.test.cj2.Config;
import com.oneunit.test.cj2.R;

import java.io.IOException;

public class TestActivity extends Activity {
    private Config config;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);

        this.textView = (TextView) findViewById(R.id.text_test);
        String strArr[] = new String[0];
        try {
            this.config = new Config(this);
            strArr = this.config.getValues();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        String str = "";

        for(int i = 0; i < strArr.length; i++){
            str+=(strArr[i] + "\n");
        }
        this.textView.setText(str);

    }
}
